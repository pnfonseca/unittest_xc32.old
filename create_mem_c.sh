#!/bin/bash

# Script to generate a file, called mem.c, containing the necessary declarations to create variables 
# in memory mapping the PIC microcontroller registers. These variables allow unit tests to
# be performed in a PC. 
#
# The script is prepared to run under Linux or Cygwin. 
#
# pf@ua.pt


# Set according to installed XC32 version
# See PIC32_HOME variable for the possible location of XC32
XC32_VER="v1.4x"


UNAME=$(uname)

# echo $UNAME


if [[ $UNAME == "Linux" ]]; then
	PIC32_HOME="/opt/microchip"
fi

# Warning: scipt adapted to Cygwin. Not tested! May require tweaking...
if [[ UNAME == "CYGWIN_NT-6.1" ]]; then
	PIC32_HOME="/cygdrive/c/Program\ Files\ \(x86\)/Microchip"
fi


XC32_INCLUDE=$PIC32_HOME/xc32/$XC32_VER/pic32mx/include/

# echo $XC32_INCLUDE

if [[ -d $XC32_INCLUDE ]]; then
	echo "Creating file..."
	cat $XC32_INCLUDE/proc/p32mx795f512h.h  | grep "unsigned int" | sed -e  's/extern //' > mem.c
	echo "mem.c created!"
else
	echo "Error! Include path: "
	echo $XC32_INCLUDE
	echo "not found!"
fi
