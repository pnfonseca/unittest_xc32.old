
.PHONY = run

UNAME := $(shell uname)

# Uncomment one of the following to choose the proper PIC32 tools
# home dir, according to the dev environment.

# PIC32_HOME = /cygdrive/c/Program\ Files\ \(x86\)/Microchip
ifeq ($(UNAME), Linux)
PIC32_HOME = /opt/microchip
PROG = main
endif

ifeq ($(UNAME),CYGWIN_NT-6.1)
PIC32_HOME = /cygdrive/c/Program\ Files\ \(x86\)/Microchip
PROG = main.exe
endif

# Set according to installed XC32 version
# See PIC32_HOME variable for the possible location of XC32
XC32_VER=v1.42

CC = gcc
CFLAGS +=  -D__32MX795F512H__ -D__LANGUAGE_C__
CPPFLAGS += -I $(PIC32_HOME)/xc32/$(XC32_VER)/pic32mx/include/

$(PROG) : main.o mem.o
	gcc main.o mem.o -o main

run : $(PROG)
	./$(PROG)
	
clean : 
	rm -f *.o
	rm -f *~
	rm -f $(PROG)
