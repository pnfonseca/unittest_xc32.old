/*
 * Example of how to compile and run in a PC environment code that accessing the microcontroller
 * registers.
 *
 * pf@ua.pt
 */
#include <stdio.h>
#include <string.h>

#include <xc.h>

	
const char * num_to_binary(int x, int nbits);

int main(void)
{
	printf("Usage of XC32 include files for PC code\n");	
	WDTCON = 0x00;
	printf("Initial WDTCON value hex:%x / bin:%s \n\n", WDTCON, num_to_binary(WDTCON, 16));
	
	WDTCONbits.ON=1;
	printf("Bit ON set (WDTCON value:%s) \n", num_to_binary(WDTCON, 16));
	WDTCONbits.ON=0;
	printf("Bit ON reset (WDTCON value:%s) \n", num_to_binary(WDTCON, 16));
		
	WDTCONbits.WDTCLR=1;	
	printf("Bit WDTCLR set (WDTCON value:%s) \n", num_to_binary(WDTCON, 16));
	WDTCONbits.WDTCLR=0;
	printf("Bit  WDTCLR reset (WDTCON value:%s) \n", num_to_binary(WDTCON, 16));
	
	WDTCONbits.WDTPSTA = 0x1F;	
	printf("Field WDTPSTA set to all 1's (WDTCON value:%s) \n", num_to_binary(WDTCON, 16));
	WDTCONbits.WDTPSTA=0;
	printf("Field WDTPSTA reset to 0's (WDTCON value:%s) \n", num_to_binary(WDTCON, 16));


	
	return 0;
}

const char * num_to_binary(int x, int nbits)
{
    static char bin_str[65];
    int z, i=1;
    
    bin_str[0] = '\0';	    
    
    for (z = 1 << (nbits -1); z > 0; z >>= 1)
    {    		
      strcat(bin_str, ((x & z) == z) ? "1" : "0");
    	if((i++ % 4) == 0)
    		strcat(bin_str,"-");   
    }

    return bin_str;
}